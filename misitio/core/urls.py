from django.urls import path
from .views import home, form, envio

urlpatterns = [
    path('', home, name="home"),
    path('form/', form, name="form"),
    path('envio/', envio, name="envio"),
]
